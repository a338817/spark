import static spark.Spark.*;

public class Main {
    public static void main(String[] args) {
        get("/hello", (req, res) -> "Hola mundo desde GET");
        
        post("/hello", (req,res) -> "Hola mundo desde POST");
        
        put("/hello", (req,res) -> "Hola mundo desde PUT");
        
        patch("/hello", (req,res) -> "Hola mundo desde PATCH");
        
        delete("/hello", (req,res) -> "Hola mundo desde DELETE");
        
    }
}

